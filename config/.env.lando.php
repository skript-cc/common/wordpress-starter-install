<?php

use \Skript\WpInstaller\Utils\DotEnv;

$envvars = '';

/**
 * Map lando's environment info to WP_ env vars
 */
$lando = json_decode(getenv('LANDO_INFO') ?: '{}', true);

// database settings
if (isset($lando['database'])) {
    $db=$lando['database'];
    $envvars .= <<<ENV
WP_DB_NAME="{$db['creds']['database']}"
WP_DB_USER="{$db['creds']['user']}"
WP_DB_PASSWORD="{$db['creds']['password']}"
WP_DB_HOST="{$db['internal_connection']['host']}:{$db['internal_connection']['port']}"

ENV;
}

// urls
if (isset($lando['appserver']['urls'])) {
    // get the non-https non-localhost url (i.e. http://yoursite.lndo.site, should be only one)
    $urls = array_filter(
        $lando['appserver']['urls'],
        function (string $url) { 
            return strpos($url, 'https') === false 
                && strpos($url, 'localhost') === false;
        }
    );
    $url = rtrim(array_pop($urls), '/');
    
    $envvars .= <<<ENV
    WP_SITE_URL="${url}"
    WP_HOME="${url}"
    
ENV;
}

$envvars .= <<<ENV
WP_DEBUG=1
ENV;

DotEnv::setFromString($envvars);