<?php

/**
 * Loads local environment variables from file.
 *
 * Use `vendor/bin/dump-env .env` > .env.php to generate this file
 */
@include __DIR__.'/.env.php';

/**
 * The base configuration for WordPress
 */

/**
 * Database credentials
 */
define('DB_NAME',       getenv('WP_DB_NAME'));
define('DB_USER',       getenv('WP_DB_USER'));
define('DB_PASSWORD',   getenv('WP_DB_PASSWORD'));
define('DB_HOST',       getenv('WP_DB_HOST') ?: 'localhost');
define('DB_CHARSET',    getenv('WP_DB_CHARSET') ?: 'utf8');
define('DB_COLLATE',    getenv('WP_DB_COLLATE') ?: '');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = getenv('WP_TABLE_PREFIX') ?: 'wp_';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jbl$;Ex?X_*=x^tsOdgpDYDULu](Py83D(r@R:1[ p~6hp|7i+Z6ei/p#+9y]#d*');
define('SECURE_AUTH_KEY',  'YT,lM|C>&/b]!D)uI ~QY0>RDc0d`>9K: rCF1oc_hVnwhs|mn<,_SPfu@%Y->)O');
define('LOGGED_IN_KEY',    'RH~J#n9,yB62|F,cB4|D.cz.j<erK8,7Y_Qa(>LHb{%idCcLbX v@3.2FpxiVPL5');
define('NONCE_KEY',        '|sk<)djKOZeNFfnZ?,-/7 r/0Fx/Lb!G $U! W[79DmJms5IyUUd$}#5v?BL{$O$');
define('AUTH_SALT',        '1)<tno/3No+8ZM!MB2vdNQxe}3|-TH?f(7iu:.sI;(Do>nb`I^]w}MrwfQDM$Q<e');
define('SECURE_AUTH_SALT', '?tcb(myfY]+0vq*xL ? G+f+Z2M_:yS7y:Ji?lLZ#(-^B|LZ0UWR)7C0T:%yvKP0');
define('LOGGED_IN_SALT',   'E9MJ0f|-cV;IO<qk}i3VEh:5EQ_y9%qmk)Ak+^qO}l8bvr-p-kPN =#{dUw@Cijb');
define('NONCE_SALT',       '{A^r@U!-kY4J|tZs *Ii?HCEN?|b/!DiP6~>?_oe3)A`!{+`D(,r%NQrj{F_?Z2_');


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', getenv('WP_DEBUG') ?: false);

/**
 * Updates and editor settings
 */
define('DISALLOW_FILE_EDIT', true); // disables plugin and theme editor
define('DISALLOW_FILE_MODS', true); // disables plugin and theme updates

// disables **automatic** core updates
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UPDATE_CORE', false);
