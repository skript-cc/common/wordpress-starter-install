# Your Wordpress starter installation

This is an opinionated Wordpress starter repository.

## Usage

### First time installation

Prerequisites:
* a working [lando installation](https://docs.lando.dev/basics/installation.html).

In the commands below replace `your-install-dir` with the location in which
you'd like to install wordpress.

```sh
git clone git@gitlab.com:skript-cc/common/wordpress-starter-install.git your-install-dir
cd your-install-dir
```

Now, optionally, change the name in the [.lando.yml](/.lando.yml) file from
your-wordpress-install to your own project name. When lando kicks off it will
use this name in the local domain name (http://your-wordpress-install.lndo.site)

```yml
name: your-wordpress-install
recipe: lamp
...
```

At last, just start lando to setup your local development environment.

```sh
lando start
```

After lando finishes you have a fully working lando installation running in
docker containers, manageable by lando. By default, you can login to the
Wordpress admin area with username `admin` and password `password`.